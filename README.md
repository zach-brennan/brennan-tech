# Brennan.tech

This repository houses the source files for my live website 
([brennan.tech](http://brennan.tech)). It also has an automatic 
deployment system that copies the files to my server on every 
commit. I set this up so that I can work on my website from any 
computer I'm at, and so I can keep a log of what changes I make, 
in case I end up doing something that I decide I don't like later.

#Notes
This site is no longer live. The domain subscription ran out, and it was pretty
expensive to renew, so I've let it lapse for now. These files will still be here
whenever I want it back, and it will be an easy git pull command to get it set 
back up.